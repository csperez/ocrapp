import os

imagesFiles = os.listdir(os.getcwd() + "/images/newImages")
for imageFile in sorted(imagesFiles):
	if ".jpg" not in imageFile:
		continue
	print("----------------")
	print("Processing file: " + imageFile)
	os.system("java Main -inputImage " + imageFile + " -outputJSON " + imageFile.split(".jpg")[0]+"_JSON.json")
	print("\n")


#20180207_083606.jpg
