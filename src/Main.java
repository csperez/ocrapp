
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.ArrayList;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.nio.file.Paths;


/*
- Java utility method that takes in an image file (jpg, gif, png) and outputs JSON string containing the below fields. Input file will be a restaurant receipt.
        - restaurant name
        - restaurant phone
        - website (if present)
        - total amount
        - date and time of transaction
*/

public class Main {
    private static String tesseractLocation = "/usr/local/bin/tesseract";
    private static String imagesLocation = "images/newImages/";
    private static Pattern phonePattern = Pattern.compile("([0-9]{10})|([\\(]?[0-9]{3}[\\)]?[-—]?[0-9]{3}[-—]?[0-9]{4})");
    private static Pattern dollarAmountPattern = Pattern.compile("[\\$]?[0-9]{1,3}[\\W]?\\.[\\W]?[0-9]{2}");

    public static void main(String[] args) throws Exception  {

        // Parse named parameter arguments:
        final Map<String, List<String>> params = new HashMap<>();
        List<String> options = null;
        for (int i = 0; i < args.length; i++) {
            final String a = args[i];
            if (a.charAt(0) == '-') {
                if (a.length() < 2) {
                    System.err.println("Error at argument " + a);
                    return;
                }
                options = new ArrayList<>();
                params.put(a.substring(1), options);
            }
            else if (options != null) {
                options.add(a);
            }
            else {
                System.err.println("Illegal parameter usage");
                return;
            }
        }

        // e.g.
        //inputImage: "20180116_140256.jpg"
        //outputJSON: "20180116_140256_JSON.json"
        ProcessFile(imagesLocation + params.get("inputImage").get(0), imagesLocation + params.get("outputJSON").get(0));
    }

    public static void ProcessFile(String inputFilePath, String outputFilePath) throws Exception {

        // Store the tesseract output in a temp file, named with a random int:
        Random random = new Random();
        int randIntValue = random.nextInt(1000 + 1);
        String tempOCRTextPath = imagesLocation + "tempOCR_" + Integer.toString(randIntValue);

        // Call tesseract on the input file, wait for the process to complete, and print the final exit code (0 = success):
        // Also time the execution of the tesseract program:
        String cmd_tesseract = tesseractLocation + " " + inputFilePath + " " + tempOCRTextPath + " -l eng -psm 1 --oem 2";
        ProcessBuilder pb_tesseract = new ProcessBuilder(cmd_tesseract.split(" "));
        long start = System.currentTimeMillis();
        Process p_tesseract = pb_tesseract.start();
        int exitCode_tesseract = p_tesseract.waitFor();
        long end = System.currentTimeMillis();
        NumberFormat formatter = new DecimalFormat("#0.00000");
        System.out.print("Tesseract exec time is " + formatter.format((end - start) / 1000d) + " seconds");
        System.out.println(exitCode_tesseract);

        // Read in the temporary text file output by tesseract:
        Scanner scanner = new Scanner( new File(tempOCRTextPath + ".txt") );
        String text = scanner.useDelimiter("\\A").next();
        scanner.close();
        String[] textLines = text.split("\\n");



        // Store all possible matches for each target string in arrays
        // Although only one will be returned to the user, it might be helpful to consider all matches in the future
        ArrayList<String> restaurantMatches = new ArrayList<String>();
        ArrayList<String> phoneMatches = new ArrayList<String>();
        ArrayList<String> totalAmountMatches = new ArrayList<String>();
        ArrayList<String> dollarAmountMatches = new ArrayList<String>();
        String dollarMatchAmount = new String();
        String restaurantName = "";
        String restaurantPhone = "";

        // Loop through the lines of text and store detected regex matches:
        // Look through only the first third of the file
        int textLinesLength = textLines.length;
        for (int i = 0; i < textLinesLength; i++) {
            String line = textLines[i].trim();
            if (line.length() == 0){
                continue;
            }
            // Check the first ten lines of the text file for restaurant results:
            if (i <= Math.min(10,Math.ceil(textLinesLength/4.0))) {
                // Store only lines not containing a digit. These are then the possible restaurant names.
                // Account for possible minor errors such as "O" parsed as "0".
                // Require that the name of the restaurant be of length at least 3.
                int numDigitsInLine = line.replaceAll("\\D", "").length();
                if (numDigitsInLine <= 3 && line.length() >= 3) {
                    restaurantMatches.add(line);
                }
            }


            // phone match
            Matcher phoneMatcher = phonePattern.matcher(line) ;
            while (phoneMatcher.find()){
                phoneMatches.add(phoneMatcher.group(0));
            }

            // total amount match
            String line_lowerCase = line.toLowerCase();
            if (line_lowerCase.contains("total")){
                // skip if it contains the subtotal:
                if (line_lowerCase.contains("sub")) {
                    continue;
                }
                totalAmountMatches.add(line);
            }
            // approved amount match
            if (line_lowerCase.contains("approved amount")){
                totalAmountMatches.add(line);
            }
        }

        // Assume our desired phone match is the first phone match found:
        if (phoneMatches.size() != 0) {
            restaurantPhone = phoneMatches.get(0);
        }

        // Assume our desired total match contains our desired final dollar amount:
        System.out.println("Total amount matches:");
        System.out.println(totalAmountMatches);
        for (int i = 0; i < totalAmountMatches.size(); i++) {
            String totalLineContainingDollarAmount = totalAmountMatches.get(i);
            Matcher dollarMatcher = dollarAmountPattern.matcher(totalLineContainingDollarAmount);
            while(dollarMatcher.find()) {
                dollarMatchAmount = "$" + dollarMatcher.group(0).replace(" ", "");
                dollarMatchAmount = dollarMatchAmount.replace("$$", "$");
                dollarAmountMatches.add(dollarMatchAmount);
            }
        }
        //System.out.println("Dollar amount matches:");
        //System.out.println(dollarMatchAmount);


        // Output the JSON string:
        String finalJSONOutput = "{\"receipt\":{";
        // replace newlines with space and remove quotation marks
        finalJSONOutput = finalJSONOutput + "\"unParsedTextData\":\"" + text.replace("\"","").replace("\n"," ").trim() + "\",";
        // produce matches as pipe-delimited lists:
        String restaurantNameList = "";
        for(String rName:restaurantMatches) {
            restaurantNameList = restaurantNameList + "|" + rName;
        }
        restaurantNameList = restaurantNameList.replaceFirst("\\|","");
        finalJSONOutput = finalJSONOutput + "\"restaurant\":\"" + restaurantNameList + "\",";
        String phoneList = "";
        for(String pName:phoneMatches) {
            phoneList = phoneList + "|" + pName;
        }
        phoneList = phoneList.replaceFirst("\\|","");
        finalJSONOutput = finalJSONOutput + "\"phone\":\"" + phoneList + "\",";
        String dollarList = "";
        for(String dName:dollarAmountMatches) {
            dollarList = dollarList + "|" + dName;
        }
        dollarList = dollarList.replaceFirst("\\|","");
        finalJSONOutput = finalJSONOutput + "\"totalAmount\":\"" + dollarList + "\"";
        finalJSONOutput = finalJSONOutput + "}}";
        System.out.println(finalJSONOutput);

        BufferedWriter outputWriter = new BufferedWriter(new FileWriter(outputFilePath));
        outputWriter.write(finalJSONOutput);
        outputWriter.close();

        // Delete the temp text file produced by tesseract:
        File tempFile = new File(tempOCRTextPath + ".txt");
        tempFile.delete();

    }


}

